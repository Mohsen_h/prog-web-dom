<?php
require_once('libcalcul.php');

if(isset($_POST['SubmitButton']))
{
  $somme = $_POST['somme'];
  $taux = $_POST['taux'];
  $duree = $_POST['duree'];

  if (!CheckValues($somme, $taux, $duree)) {
    echo "<script>
         alert('Please check entered values!');
         location.href = 'calcul.html';
         </script>";
  } else {
    echo "<h2>Calcul d’intérêts composés</h2>";
    echo "Résultat = " . cumul($somme, $taux, $duree) . "<br><br>";
    echo "<a href='calcul.html'> <button> Effectuer à nouveau le calcul </button> </a>";
  }
}

function CheckValues($somme, $taux, $duree)
{
  if(is_numeric($somme)==1 && is_numeric($taux)==1 && is_numeric($duree)==1)
  {
    return true;
  }
  else
  {
    return false;
  }
}

?>
